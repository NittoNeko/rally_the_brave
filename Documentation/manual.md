##Character Attributes
    Players empower themselves through better items, skills and souls, in order to defeat stronger and stronger foes.
    Attributes include:
1.	Health: When players’ health reaches zero, game is over.
2.	Mana: Primary resource used to cast spells.
3.	Max Health/Mana: Upper limit of players’ health/mana
4.	Health/Mana Steal: Health/Mana recovered per hit.
5.	Health/Mana Recovery: Health/Mana recovered per second.
6.	Attack: Damage dealt to enemies.
7.	Armor: Reduction of damage received.
8.	Critical Chance: Chance to deal extra damage
9.	Critical Multiple: How much extra damage a critical hit can cause.
10.	Thorn: Damage returned to attackers, ignoring their armors.
11.	Haste: Reduction of interval between attacks and skills’ cooldown.
12.	Focus: Reduction of skills’ costs.
13.	Movement: How fast players can move.

##Equipment
    Equipment is used to increase players’ attributes, and sometimes it can bring special effects to players.
    Equipment includes:
1.	Weapon: Decides Attack, Haste, Critical Chance, Critical Multiple and Focus
2.	Head: Decides Max Mana, Mana Steal, Mana Recovery, Focus, Critical Chance.
3.	Chest: Decides Max Health, Health Steal, Health Recovery, Thorn and Armor.
4.	Leg: Decides Health Recovery, Mana Recovery, Armor, Movement Speed, Thorn.
5.	Feet: Decides Health Steal, Mana Steal, Movement Speed, Haste, Attack.
6.	Hand: Decides Health Steal, Mana Steal, Attack, Haste and Critical Multiple.
7.	Trinket and Ring: All attributes can appear.
    Each piece of equipment can be enhanced up to five times, each time increases its attributes by 20%.

##Skill

Skills are used to damage foes, heal allies and empower players.
A character can hold several skills at one time, depending on players’ choices.
Skills include:
1.	Martial Arts: Generally, this kind of skills costs no mana and has no cooldown, but its power is relatively low.
2.	Spells: This kind of skills costs mana and has cooldowns, but its range and power are high.
3.	Aura: This kind of skills lasts for a period and drains mana during that time.

Soul
    Souls are strong passive effects including resurrection, flying and so on.
    Players can hold up to three souls at one time and can not store souls in the inventory.
    When their soul slots are full, players have to give up one for another.


Item
    Items include health potions, buff potions, equipment upgrades and so on.

Rarity
    All equipment, skills, souls can be divided into four rarities: Common, Rare, Legendary and Cursed. Generally Cursed is the best quality players can find but at a price.
    
Status
    Status includes common effects called buff and debuff like increasing players’ attributes, and special effects like loss of control.
    Since special effects are powerful, it has its limitations: the same special effects can only apply to a single target several times in a certain time, and they never stack.
    


Merchant
    Before entering a level, players could buy starter items from merchants.
    Players would also find merchants during adventures. Their prices are high, but sometimes players would have to make choices between wealth and life.

Currency
    Players make money by killing monsters, looting treasures and selling items to merchants.
    Upon death, half amount of money will be lost. So, spend it before it’s too late.

