﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Information of an attacker
public interface IThornable
{
    void TakeThorn(float value);
}
