﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ESpecialStatusType
{
    Snared, Unwise, Coward, Stunned, Purified, Unstoppable, Immortal
}
