﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// A consistent status means it comes from sources like equipment or souls
public enum EStatusType
{
    Buff, Debuff, Special, Consistent
}
