﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// Independent should multiply each other and then multiply base value
// Prefix with Mult should sum up and then multiply base value
// Additive should sum up and then add to base value
public enum EAttrModLayer
{
    Independent, Additive, Mult1, Mult2, Mult3
}
