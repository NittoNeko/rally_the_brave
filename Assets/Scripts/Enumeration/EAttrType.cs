﻿

[System.Serializable]
public enum EAttrType
{
    MaxVitality, VitalityRec, VitalitySteal,
    MaxPriRes, PriResRec, PriResSteal,
    Attack, Haste, CritRate, CritMult,
    Focus, Armor, Thorn, MoveSpeed
}