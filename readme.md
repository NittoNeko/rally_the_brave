# Not finished yet, still in development

##Overview

Rally the Brave is a 2D roguelike game developed with Unity and written in c#, where players travel between randomly generated levels, discover new contents and collect fantastic items, skills and souls to defeat challenging foes.


#Rally the Brave

    Rally the Brave is a 2D roguelike game developed personally by Yajie Hao using Unity engine.
    
    In this game, players will act as an adventurer who dares to enter an undiscovered land,
    find fantastic treasures and defeat unknown creatures.
    
    In order to fulfill your curiosity, levels are randomly generated and as you go deeper and deeper,
    the world becomes more challenging. In the same time, players empower themselves with legendary spells, mystic equipments
    and cursed but awesome souls to face their indefinite fate.

#Notice

    This is an experimental project, through which I'm training myself for game development, 
    getting familar with game programming and preparing myslef for job application.
    
    With that said, this project is still in development and the progress is fairly slow
    because I'd like to try different coding styles, architecture designs and data structures,
    in order to find the most effective way of coding in game development.
    